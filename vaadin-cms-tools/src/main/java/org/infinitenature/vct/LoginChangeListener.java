package org.infinitenature.vct;

@FunctionalInterface
public interface LoginChangeListener {
  void loginChanged();
}
