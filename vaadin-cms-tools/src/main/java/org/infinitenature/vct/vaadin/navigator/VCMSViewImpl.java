package org.infinitenature.vct.vaadin.navigator;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.text.StringSubstitutor;
import org.infinitenature.vct.TitleUpdateListener;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.vct.VCMSLayout;
import org.infinitenature.vct.VCMSSeoDetailsProvider;
import org.infinitenature.vct.VCMSTitleParameterProvider;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.ui.Composite;

public class VCMSViewImpl extends Composite implements VCMSView, TitleUpdateListener {
  private String title;
  private VCMSLayout layout;
  private VCMSTitleParameterProvider titleParameterProvider;
  private VCMSSeoDetailsProvider seoDetailsProvider;

  public VCMSViewImpl(String title) {
    this.title = title;
  }

  @Override
  public void enter(ViewChangeEvent event) {
    setCompositionRoot(layout.getVaadinComponent());
    layout.enter(event);
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public void addComponent(String area, VCMSComponent component) {
    layout.addComponent(area, component, false);
  }

  @Override
  public void addMainComponent(String area, VCMSComponent component) {
    layout.addComponent(area, component, true);
  }

  @Override
  public void onTitleParameterUpdate() {
    String titleWithParameters = computeTitle();
    Page.getCurrent().setTitle(titleWithParameters);
  }

  @Override
  public String computeTitle() {
    Map<String, String> titleParamters =
        new HashMap<>(titleParameterProvider.getTitleParameters());
    titleParamters.put("APP_NAME", VCMSContext.getCurrent().getApp().getName());
    return new StringSubstitutor(titleParamters).replace(getTitle());
  }


  public void setLayout(VCMSLayout layout) {
    this.layout = layout;

  }

  public void setTitleParameterProvider(VCMSTitleParameterProvider titleParameterProvider) {
    this.titleParameterProvider = titleParameterProvider;
  }

  public void setSeoDetailsProvider(VCMSSeoDetailsProvider seoDetailsProvider) {
    this.seoDetailsProvider = seoDetailsProvider;
  }

  @Override
  public boolean equals(Object obj) {
    // use vaadin implementation
    return super.equals(obj);
  }

  @Override
  public int hashCode() {
    // use vaadin implementation
    return super.hashCode();
  }
}
