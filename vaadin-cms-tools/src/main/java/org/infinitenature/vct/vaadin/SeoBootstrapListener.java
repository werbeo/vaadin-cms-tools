/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */
package org.infinitenature.vct.vaadin;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.vct.VCMSSeoDetailsProvider;
import org.infinitenature.vct.vaadin.navigator.PageProvider;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.vaadin.server.BootstrapFragmentResponse;
import com.vaadin.server.BootstrapListener;
import com.vaadin.server.BootstrapPageResponse;
import com.vaadin.server.VaadinRequest;

/**
 *
 * @author mattitahvonenitmill
 */
public class SeoBootstrapListener implements BootstrapListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(SeoBootstrapListener.class);
  private final PageProvider pageProvider;


  public SeoBootstrapListener(PageProvider pageProvider) {
    this.pageProvider = pageProvider;
  }

  @Override
  public void modifyBootstrapFragment(BootstrapFragmentResponse response) {
    // NOOP
  }

  @Override
  public void modifyBootstrapPage(BootstrapPageResponse response) {
    SeoDetails seoDetails = getSeoDetails(response);

    String path = computePath(response);
    LOGGER.info("PATH: {}", path);
    addHeaders(path, response, seoDetails);

  }

  private void addHeaders(String path, BootstrapPageResponse response, SeoDetails details) {
    Optional<String> seoImage = details.getSeoImage();
    Optional<String> seoDescription = details.getSeoDescription();
    // Generic

    if (seoDescription.isPresent()) {
      meta(response, "description", seoDescription.get());
    }

    // For Twitter
    meta(response, "twitter:card", "summary");
    meta(response, "twitter:site", path + details.getSeoUrl());
    meta(response, "twitter:title", details.getSeoTitle());
    if (seoDescription.isPresent()) {
      meta(response, "twitter:description", seoDescription.get());
    }

    if (seoImage.isPresent()) {
      meta(response, "twitter:image", seoImage.get());
    }

    // For Facebook
    Element html = response.getDocument().getElementsByTag("html").get(0);
    html.attr("prefix", "og: http://ogp.me/ns# article: http://ogp.me/ns/article#");
    metaProperty(response, "og:type", "website");
    metaProperty(response, "og:url", path + details.getSeoUrl());
    metaProperty(response, "og:title", details.getSeoTitle());
    if (seoDescription.isPresent()) {
      metaProperty(response, "og:description", seoDescription.get());
    }
    if (seoImage.isPresent()) {
      metaProperty(response, "og:image", seoImage.get());
    }
  }

  private SeoDetails getSeoDetails(BootstrapPageResponse response) {
    return addPageSeoDetails(response, getAppWideSeoDetails());
  }

  private SeoDetails addPageSeoDetails(BootstrapPageResponse response, SeoDetails seoDetails) {
    HttpServletRequest request = (HttpServletRequest) response.getRequest();
    String viewAndParameters =
        request.getRequestURI().startsWith("/") ? request.getRequestURI().substring(1)
            : request.getRequestURI();
    LOGGER.info("servlet path {}, uri {}", request.getServletPath(), viewAndParameters);
    String viewName = pageProvider.getViewName(viewAndParameters);
    String parameters =
        viewAndParameters.startsWith(viewName) ? viewAndParameters.substring(viewName.length() + 1)
            : "";

    Optional<VCMSSeoDetailsProvider> optionalSeoDetailsProvider =
        pageProvider.getSeoDetailsProvider(viewName);

    if (optionalSeoDetailsProvider.isPresent()) {
      VCMSSeoDetailsProvider seoDetailsProvider = optionalSeoDetailsProvider.get();
      seoDetailsProvider.setInitialParameters(parameters);

      Optional<String> seoDescription = seoDetailsProvider.getSeoDescription();
      if (seoDescription.isPresent()) {
        seoDetails.setSeoDescription(seoDescription);
      }

      Optional<String> seoImageURL = seoDetailsProvider.getSeoImageURL();
      if (seoImageURL.isPresent()) {
        seoDetails.setSeoImage(seoImageURL);
      }

      Optional<String> seoURL = seoDetailsProvider.getSeoURL();
      if (seoURL.isPresent()) {
        seoDetails.setSeoUrl("/" + seoURL.get());
      } else {
        seoDetails.setSeoUrl("/" + viewAndParameters);
      }

      Optional<String> seoTitle = seoDetailsProvider.getSeoTitle();
      if (seoTitle.isPresent()) {
        seoDetails.setSeoTitle(seoTitle.get());
      }
    }
    return seoDetails;
  }

  private SeoDetails getAppWideSeoDetails() {
    SeoDetails seoDetails = new SeoDetails();
    seoDetails.setSeoUrl("/");
    VCMSContext context = VCMSContext.getCurrent();

    if (context == null) {
      LOGGER.warn("No context found.");
    } else {
      seoDetails.setSeoTitle(context.getApp().getName());
      seoDetails.setSeoDescription(context.getApp().getDescription());
      seoDetails.setSeoImage(context.getApp().getImageURL());
    }
    return seoDetails;
  }

  private void meta(BootstrapPageResponse response, String name, String content) {
    Element head = response.getDocument().getElementsByTag("head").get(0);
    Element meta = response.getDocument().createElement("meta");
    meta.attr("name", name);
    meta.attr("content", content);
    head.appendChild(meta);
  }

  private void metaProperty(BootstrapPageResponse response, String name, String content) {
    Element head = response.getDocument().getElementsByTag("head").get(0);
    Element meta = response.getDocument().createElement("meta");
    meta.attr("property", name);
    meta.attr("content", content);
    head.appendChild(meta);
  }
  private String computePath(BootstrapPageResponse response) {
    VaadinRequest vaadinRequest = response.getRequest();
    HttpServletRequest request = (HttpServletRequest) vaadinRequest;
    String scheme = request.getScheme();
    String server = request.getServerName();
    int port = request.getServerPort();
    StringBuilder builder = new StringBuilder(scheme).append("://").append(server);
    if (port != 80) {
      builder.append(':').append(port);
    }
    return builder.toString();
  }
}
