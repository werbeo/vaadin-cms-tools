package org.infinitenature.vct.vaadin;

import java.util.Optional;

public class SeoDetails {
  private String seoUrl;
  private String seoTitle;
  private Optional<String> seoImage = Optional.empty();
  private Optional<String> seoDescription = Optional.empty();

  public String getSeoUrl() {
    return seoUrl;
  }

  public void setSeoUrl(String seoUrl) {
    this.seoUrl = seoUrl;
  }

  public String getSeoTitle() {
    return seoTitle;
  }

  public void setSeoTitle(String seoTitle) {
    this.seoTitle = seoTitle;
  }

  public Optional<String> getSeoImage() {
    return seoImage;
  }

  public void setSeoImage(Optional<String> seoImage) {
    this.seoImage = seoImage;
  }

  public Optional<String> getSeoDescription() {
    return seoDescription;
  }

  public void setSeoDescription(Optional<String> seoDescription) {
    this.seoDescription = seoDescription;
  }
}
