package org.infinitenature.vct.vaadin;

import org.infinitenature.vct.VCMSContext;
import org.infinitenature.vct.ViewMode;
import com.vaadin.server.Page.BrowserWindowResizeEvent;
import com.vaadin.server.Page.BrowserWindowResizeListener;

public abstract class AbstractViewModeDetector implements BrowserWindowResizeListener {
  public abstract ViewMode getNewViewMode(BrowserWindowResizeEvent event);

  @Override
  public void browserWindowResized(BrowserWindowResizeEvent event) {
    ViewMode newViewMode = getNewViewMode(event);
    if (newViewMode != VCMSContext.getCurrent().getViewMode()) {

      VCMSContext.getCurrent().setViewMode(newViewMode);
      VCMSContext.getCurrent().notifyViewModeChanged();
    }
  }
}
