package org.infinitenature.vct.vaadin.navigator;

import java.util.Collections;
import java.util.Optional;
import org.infinitenature.vct.Authentication;
import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSErrorView;
import org.infinitenature.vct.VCMSLayout;
import org.infinitenature.vct.VCMSSeoDetailsProvider;
import org.infinitenature.vct.description.VCMSComponentDescription;
import org.infinitenature.vct.description.VCMSPageDescription;
import org.infinitenature.vct.error.NotLoggedInException;
import org.infinitenature.vct.repository.ComponentFactory;
import org.infinitenature.vct.repository.PageRepository;
import org.infinitenature.vct.theme.VCMSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class PageProvider implements ViewProvider {
  private static final Logger LOGGER = LoggerFactory.getLogger(PageProvider.class);

  private ComponentFactory componentFactory;
  private PageRepository repository;
  private Optional<String> forceViewName = Optional.empty();
  private ViewProvider errorViewProvider = new ViewProvider() {

    @Override
    public String getViewName(String viewAndParameters) {
      return "error";
    }

    @Override
    public View getView(String viewName) {
      return getPage(viewName);
    }
  };

  public PageProvider() {
    super();
  }

  public PageProvider(PageRepository repository) {
    this();
    this.repository = repository;
  }

  @Override
  public String getViewName(String viewAndParameters) {
    if (forceViewName.isPresent()) {
      return forceViewName.get();
    } else {
      return repository.getViewName(getApp(), viewAndParameters);
    }
  }

  private VCMSApp getApp() {
    return VCMSUtil.getCurrentContext().getApp();
  }

  public VCMSView getPage(String viewName) {
    try {
      VCMSPageDescription pageDescription = getPageDescription(viewName);
    
      return createInstance(pageDescription);
    } catch (IllegalArgumentException e) {
      LOGGER.info("Can't get page for {}, returning error view", viewName, e);
      return new VCMSErrorView();
    }
  }

  public Optional<VCMSSeoDetailsProvider> getSeoDetailsProvider(String viewName) {
    try {
      VCMSPageDescription pageDescription = getPageDescription(viewName);
      return getSeoDetailsProvider(pageDescription);
    } catch (Exception e) {
      LOGGER.info("Failure getting seo details for view {}", viewName, e);
      return Optional.empty();
    }
  }

  private Optional<VCMSSeoDetailsProvider> getSeoDetailsProvider(
      VCMSPageDescription pageDescription) {
    for (String area : pageDescription.getComponents().keySet()) {
      for (VCMSComponentDescription componentDescription : pageDescription.getComponents()
          .get(area)) {
        VCMSComponent component =
            createInstance(componentDescription);
        if (isMainComponentForPage(component, pageDescription)) {
          component.nonUIinit(componentDescription.getParameter());
          return Optional.of(component);
        }
      }
    }
    return Optional.empty();
  }

  private boolean isMainComponentForPage(VCMSComponent component,
      VCMSPageDescription pageDescription) {
    return component.getClass().getName().equals(pageDescription.getMainComponentClassName());
  }

  private VCMSPageDescription getPageDescription(String viewName) {
    VCMSPageDescription pageDescription = repository.getDescription(getApp(), viewName);
    if (!pageDescription.isAnonymousAllowed()
        && VCMSUtil.getCurrentContext().getAuthentication().equals(Authentication.ANONYMOUS)) {
      throw new NotLoggedInException("Anonymous access to " + viewName + " is not allowed.");
    }
    return pageDescription;
  }

  private VCMSViewImpl createInstance(VCMSPageDescription pageDescription) {
    VCMSViewImpl vcmsView = new VCMSViewImpl(pageDescription.getTitle());
    VCMSLayout layout = null;

    layout = componentFactory.getLayout(pageDescription.getLayout());

    layout.init(Collections.emptyMap());
    vcmsView.setLayout(layout);

    int mainComponentsCount = 0;
    for (String area : pageDescription.getComponents().keySet()) {
      for (VCMSComponentDescription componentDescription : pageDescription.getComponents()
          .get(area)) {
        VCMSComponent component =
            createInstance(componentDescription);
        component.nonUIinit(componentDescription.getParameter());

        component.init(componentDescription.getParameter());

        if (isMainComponentForPage(component, pageDescription)) {
          vcmsView.addMainComponent(area, component);
          component.setTitleUpdateListener(vcmsView);
          vcmsView.setTitleParameterProvider(component);
          vcmsView.setSeoDetailsProvider(component);
          mainComponentsCount++;
        } else {
          vcmsView.addComponent(area, component);
        }
      }
    }
    if (mainComponentsCount != 1) {
      LOGGER.warn("Found {} main components for page {} with URL {}", mainComponentsCount,
          pageDescription.getTitle(), pageDescription.getUrlPrefix());
    }
    return vcmsView;
  }

  private VCMSComponent createInstance(VCMSComponentDescription componentDescription) {
    return componentFactory.get(componentDescription.getImplementationClass());
  }

  public PageRepository getRepository() {
    return repository;
  }

  public void setRepository(PageRepository repository) {
    this.repository = repository;
  }

  @Override
  public View getView(String viewName) {
    VCMSView page = getPage(viewName);
    page.onTitleParameterUpdate();
    return page;
  }

  public ViewProvider getErrorViewProvider() {
    return errorViewProvider;
  }

  public void setComponentFactory(ComponentFactory componentFactory) {
    this.componentFactory = componentFactory;
  }

  /**
   * Define a viewName which is always returned if present
   *
   * @param viewName the view name to return
   */
  public void setForceViewName(Optional<String> viewName) {
    this.forceViewName = viewName;
  }


}
