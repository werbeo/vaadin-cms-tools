package org.infinitenature.vct.vaadin.navigator;

import org.infinitenature.vct.VCMSComponent;
import com.vaadin.navigator.View;

public interface VCMSView extends View {

  public String getTitle();

  public void addComponent(String area, VCMSComponent component);

  public void addMainComponent(String area, VCMSComponent component);

  public String computeTitle();

  public void onTitleParameterUpdate();
}
