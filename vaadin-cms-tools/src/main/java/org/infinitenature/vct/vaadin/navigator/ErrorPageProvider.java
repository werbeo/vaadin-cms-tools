package org.infinitenature.vct.vaadin.navigator;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class ErrorPageProvider implements ViewProvider {

  @Override
  public String getViewName(String viewAndParameters) {
    return "error";
  }

  @Override
  public View getView(String viewName) {
    return null;
  }

}
