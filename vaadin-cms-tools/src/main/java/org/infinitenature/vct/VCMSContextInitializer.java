package org.infinitenature.vct;

import org.infinitenature.vct.repository.AppRepositoryQueries;
import org.infinitenature.vct.theme.VCMSUtil;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.VaadinSession;

/**
 * Creates, initalizes and stores a {@link VCMSContext} for the application. The {@link VCMSContext}
 * is stored in the {@link VaadinSession} attributes with the key {@link VCMSContext#SESSION_KEY}
 * 
 * @author dve
 *
 */
@SuppressWarnings("serial")
public class VCMSContextInitializer implements SessionInitListener {
  private AppRepositoryQueries appRepository;

  @Override
  public void sessionInit(SessionInitEvent event) throws ServiceException {
    VCMSContext context = createContextInstance(event);
    String hostName = VCMSUtil.getHostName(event.getRequest());
    context.setApp(appRepository.getApp(hostName));
    ViewMode forcedViewMode = appRepository.getForcedViewMode(context.getApp(), hostName);
    if (forcedViewMode != null) {
      context.setViewMode(forcedViewMode);
      context.setForceViewMode(true);
    }
    intiContext(context);
    VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);
  }

  /**
   * Overridde this method to a extra attributes to the {@link VCMSContext}. * @param context
   */
  protected void intiContext(VCMSContext context) {
    // A palaceholder to be overriden

  }

  /**
   * Creates a new instance of the {@link VCMSContext}. This method should be overridden if a custom
   * implementation of {@link VCMSContext} is used.
   * 
   * @param event the vaadin event
   * @return the {@link VCMSContext} instance
   */
  protected VCMSContext createContextInstance(SessionInitEvent event) {
    return new VCMSContext();
  }

  public void setAppRepository(AppRepositoryQueries appRepository) {
    this.appRepository = appRepository;
  }
}
