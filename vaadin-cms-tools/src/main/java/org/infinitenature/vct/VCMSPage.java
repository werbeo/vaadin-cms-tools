package org.infinitenature.vct;

/**
 * Representing a page for vaadin mini cms.
 *
 * @author dve
 *
 */
public class VCMSPage {
  private VCMSTemplate template;
  private String uri;

  public VCMSTemplate getTemplate() {
    return template;
  }

  public void setTemplate(VCMSTemplate template) {
    this.template = template;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

}
