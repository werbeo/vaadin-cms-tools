package org.infinitenature.vct;

import java.util.List;
import java.util.Map;
import org.infinitenature.vct.description.VCMSComponentDescriptionXML;

public interface VCMSTemplate {
  String getLayout();

  Map<String, List<VCMSComponentDescriptionXML>> getComponents();
}
