package org.infinitenature.vct;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public interface Authentication {
  public default String getPrincipal() {
    return "";
  }

  public default List<String> getRoles() {
    return Collections.emptyList();
  }

  public default boolean isAnonymous() {
    return true;
  }

  public default Optional<String> getFamilyName() {
    return Optional.empty();
  }

  public default Optional<String> getGivenName() {
    return Optional.empty();
  }

  public default Optional<String> getEmail() {
    return Optional.empty();
  }

  public default Object getOriginalAuthentication() {
    return null;
  }

  public static final Authentication ANONYMOUS = new Authentication() {};
}
