package org.infinitenature.vct;

import org.infinitenature.vct.vaadin.navigator.VCMSView;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;

public class VCMSErrorView extends CustomComponent implements VCMSView {

  @Override
  public void enter(ViewChangeEvent event) {
    setCompositionRoot(new Label("Ups, something went wrong!"));

  }

  @Override
  public String getTitle() {
    return computeTitle();
  }

  @Override
  public String computeTitle() {
    return "An error occured";
  }

  @Override
  public void addComponent(String area, VCMSComponent component) {
    throw new IllegalArgumentException("No components in eror view yet!");
  }

  @Override
  public void addMainComponent(String area, VCMSComponent component) {
    throw new IllegalArgumentException("No components in eror view yet!");
  }

  @Override
  public void onTitleParameterUpdate() {
    // NOOP
  }
}
