package org.infinitenature.vct;

import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class VCMSApp {
  private String name;
  private Optional<String> imageURL;
  private Optional<String> description;

  public VCMSApp() {
    super();
    this.imageURL = Optional.empty();
    this.description = Optional.empty();
  }

  public VCMSApp(String name) {
    this();
    this.name = name;
  }

  public VCMSApp(String name, String imageURL, String description) {
    this(name);
    this.imageURL = Optional.of(imageURL);
    this.description = Optional.of(description);
  }

  public Optional<String> getImageURL() {
    return imageURL;
  }

  public void setImageURL(Optional<String> imageURL) {
    this.imageURL = imageURL;
  }

  public Optional<String> getDescription() {
    return description;
  }

  public void setDescription(Optional<String> description) {
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public int hashCode() {
    return VCMSAppBeanUtil.doToHashCode(this);
  }

  @Override
  public boolean equals(Object obj) {
    return VCMSAppBeanUtil.doEquals(this, obj);
  }

  @Override
  public String toString() {
    return VCMSAppBeanUtil.doToString(this);
  }

  public void setDescription(String description) {
    if (StringUtils.isNotBlank(description)) {
      this.description = Optional.of(description);
    } else {
      this.description = Optional.empty();
    }
  }

}
