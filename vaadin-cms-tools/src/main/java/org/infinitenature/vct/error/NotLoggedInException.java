package org.infinitenature.vct.error;

public class NotLoggedInException extends VCTException {

  public NotLoggedInException(String message) {
    super(message);
  }

}
