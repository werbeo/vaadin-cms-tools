package org.infinitenature.vct.error;

public class VCTException extends RuntimeException {

  public VCTException(String message, Throwable cause) {
    super(message, cause);
  }

  public VCTException(String message) {
    super(message);
  }
}
