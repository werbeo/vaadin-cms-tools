package org.infinitenature.vct;

@FunctionalInterface
public interface TitleUpdateListener {
  public void onTitleParameterUpdate();
}
