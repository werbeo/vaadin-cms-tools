package org.infinitenature.vct;

import java.util.Collections;
import java.util.Map;

public interface VCMSTitleParameterProvider {
  public default void setTitleUpdateListener(TitleUpdateListener titleUpdateListener) {
  }

  public default Map<String, String> getTitleParameters() {
    return Collections.emptyMap();
  }

}
