package org.infinitenature.vct;

@FunctionalInterface
public interface ContextChangeListener {
  public void contextChanged();
}
