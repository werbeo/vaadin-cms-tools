package org.infinitenature.vct;

public abstract class ViewMode {
  protected ViewMode() {}

  @Override
  public String toString() {
    return getClass().getSimpleName();
  }
}
