package org.infinitenature.vct;

import java.util.Optional;

public interface VCMSSeoDetailsProvider {

  public default void setInitialParameters(String parameters) {
  }

  public default Optional<String> getSeoDescription() {
    return Optional.empty();
  }

  public default Optional<String> getSeoImageURL() {
    return Optional.empty();
  }

  public default Optional<String> getSeoURL() {
    return Optional.empty();
  }

  public default Optional<String> getSeoTitle() {
    return Optional.empty();
  }
}
