package org.infinitenature.vct.repository;

import java.util.Collections;
import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.ViewMode;
import org.infinitenature.vct.description.VCMSAppTemplateDescription;
import org.infinitenature.vct.description.VCMSComponentDescription;

public class AppTemplateFactory {

  public AppTemplateFactory(AppTemplateRepositoryQueries appTemplateRepositoryQueries,
      ComponentFactory componentFactory) {
    this.appTemplateRepositoryQueries = appTemplateRepositoryQueries;
    this.componentFactory = componentFactory;
  }

  private ComponentFactory componentFactory;
  private AppTemplateRepositoryQueries appTemplateRepositoryQueries;

  public VCMSAppTemplate getVCMSAppTemplate(VCMSApp app, ViewMode viewMode) {

    VCMSAppTemplateDescription appTemplateDescription =
        appTemplateRepositoryQueries.getAppTemplate(app, viewMode);

    VCMSAppTemplate appTemplate =
        componentFactory.getAppTemplate(appTemplateDescription.getImplementationClass());

    for (String area : appTemplateDescription.getStaticComponents().keySet()) {
      for (VCMSComponentDescription componentDescription : appTemplateDescription
          .getStaticComponents().get(area)) {
        VCMSComponent component =
            componentFactory.get(componentDescription.getImplementationClass());
        component.init(componentDescription.getParameter());
        appTemplate.addTemplateComponent(area, component);
      }
    }
    appTemplate.init(Collections.emptyMap());
    return appTemplate;
  }
}
