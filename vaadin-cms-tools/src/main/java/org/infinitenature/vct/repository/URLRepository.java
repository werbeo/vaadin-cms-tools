package org.infinitenature.vct.repository;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.VCMSComponent;

@FunctionalInterface
public interface URLRepository {
  public String getFragment(VCMSApp app, Class<? extends VCMSComponent> mainComponentClass);
}
