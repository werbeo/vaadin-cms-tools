package org.infinitenature.vct.repository;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.description.VCMSPageDescription;

public interface PageRepository extends URLRepository {

  VCMSPageDescription getDescription(VCMSApp app, String viewName) throws IllegalArgumentException;

  String getViewName(VCMSApp app, String viewAndParameters);

}
