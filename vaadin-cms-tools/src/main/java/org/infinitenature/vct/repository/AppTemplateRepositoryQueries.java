package org.infinitenature.vct.repository;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.ViewMode;
import org.infinitenature.vct.description.VCMSAppTemplateDescription;

public interface AppTemplateRepositoryQueries {
  VCMSAppTemplateDescription getAppTemplate(VCMSApp app, ViewMode viewMode);
}
