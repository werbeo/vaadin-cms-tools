package org.infinitenature.vct.repository;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.ViewMode;

public interface AppRepositoryQueries {

  /**
   * Returns the {@link VCMSApp} for a host name.
   * 
   * @param hostName
   * @return
   */
  VCMSApp getApp(String hostName);

  /**
   * Returns the force {@link ViewMode} for an app and host name. Override if you wand a forced
   * {@link ViewMode} for eg. host names like m.my-app.net.
   * 
   * The default implementation returns null.
   * 
   * @param app
   * @param hostName
   * @return the default {@link ViewMode} is null, which means there is no forced {@link ViewMode}.
   */
  default ViewMode getForcedViewMode(VCMSApp app, String hostName) {
    return null;
  }

}
