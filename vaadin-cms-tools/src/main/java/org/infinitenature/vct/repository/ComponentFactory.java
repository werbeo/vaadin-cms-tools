package org.infinitenature.vct.repository;

import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSLayout;

public interface ComponentFactory {

  VCMSAppTemplate getAppTemplate(String appTemplateImplementationClass);

  VCMSComponent get(String componentImplementationClass);

  VCMSLayout getLayout(String layoutImplementationClass);

  <T extends VCMSComponent> T getComponent(Class<T> clazz);
}
