package org.infinitenature.vct.repository;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.ViewMode;

public interface AppRepositoryCommands {
  public void storeApp(VCMSApp app);

  public default void addHostToApp(VCMSApp app, String host) {
    addHostToApp(app, host, null);
  }

  public void addHostToApp(VCMSApp app, String host, ViewMode forcedViewMode);
}
