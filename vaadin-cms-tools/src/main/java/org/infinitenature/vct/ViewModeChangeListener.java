package org.infinitenature.vct;

@FunctionalInterface
public interface ViewModeChangeListener {
  public void viewModeChanged(ViewMode newViewMode);
}
