package org.infinitenature.vct;

import com.vaadin.navigator.ViewDisplay;

public interface VCMSAppTemplate extends VCMSComponent, ViewDisplay {

  public void addTemplateComponent(String area, VCMSComponent component);

}
