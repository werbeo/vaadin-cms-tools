package org.infinitenature.vct;

import java.util.Map;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Component;

public interface VCMSComponent extends VCMSTitleParameterProvider, VCMSSeoDetailsProvider {
  public Component getVaadinComponent();

  /**
   * Called after construction of the component. No UI session or context is available at this time.
   * <p>
   * This method is called one time on the lifecycle of the component.
   * 
   * @param parameter
   */
  public default void nonUIinit(Map<String, String> parameter) {
  }

  /**
   * Called when the general UI should be initialized.
   * <p>
   * This method is called one time on the lifecycle of the component. It is always called after
   * {@link #nonUIinit(Map)}.
   *
   * @param parameter parameters which are only set one time during the lifecylcle
   */
  public void init(Map<String, String> parameter);

  /**
   * Called when the UI enters the component
   * <p>
   * This method can be called more times during the lifecylce of the component. It is always called
   * after {@link #init(Map)}
   *
   * @param event
   */
  public default void enter(ViewChangeEvent event) {
  }

}
