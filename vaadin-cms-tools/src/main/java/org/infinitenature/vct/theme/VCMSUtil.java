package org.infinitenature.vct.theme;

import javax.servlet.ServletRequest;
import org.infinitenature.vct.VCMSContext;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServletRequest;

public class VCMSUtil {

  public static VCMSContext getCurrentContext() {
    return VCMSContext.getCurrent();

  }

  public static String getHostName(VaadinRequest vaadinRequest) {
    try {
      VaadinServletRequest vaadinServletRequest = (VaadinServletRequest) vaadinRequest;
      ServletRequest servletRequest = vaadinServletRequest.getHttpServletRequest();

      String hostName = servletRequest.getServerName();

      return hostName;
    } catch (ClassCastException e) {
      throw new IllegalStateException(
          "This is only implemented for servlet use. Not for portlet use!", e);
    }
  }

}
