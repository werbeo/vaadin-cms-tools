package org.infinitenature.vct.theme;

import com.vaadin.server.DefaultUIProvider;
import com.vaadin.server.UICreateEvent;
import com.vaadin.server.VaadinRequest;

/**
 * Extracts the host name from a {@link VaadinRequest} and selects a theme depending on the host
 * name.
 * <p>
 * Thanks to user <a href="http://stackoverflow.com/a/39849197/2602034">"Marco C" on
 * stackoverflow.com</a>
 * 
 * @author dve
 *
 */
public abstract class ThemeByHostnameUIProvider extends DefaultUIProvider {
  @Override
  public String getTheme(UICreateEvent uiCreateEvent) {
    return getThemeName(VCMSUtil.getHostName(uiCreateEvent.getRequest()));
  }

  /**
   * Returns the theme name for the given host name.
   * 
   * @param hostName the host name
   * @return the theme name
   */
  public abstract String getThemeName(String hostName);
}
