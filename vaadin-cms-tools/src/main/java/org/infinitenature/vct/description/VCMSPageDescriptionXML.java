package org.infinitenature.vct.description;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement(name = "pageDescription")
public class VCMSPageDescriptionXML implements VCMSPageDescription {
  @XmlAttribute(name = "urlPrefix")
  private String urlPrefix;
  @XmlAttribute(name = "layout")
  private String layout;
  @XmlElement(name = "components")
  private Map<String, List<VCMSComponentDescriptionXML>> components = new HashMap<>();
  @XmlElement(name = "title")
  private String title;
  @XmlElement(name = "mainComponent")
  private String mainComponentClassName = null;
  @XmlElement(name = "anonAccessAllowed")
  private boolean anonymousAllowed = true;

  @Override
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public String getUrlPrefix() {
    return urlPrefix;
  }

  public void setUrlPrefix(String urlPrefix) {
    this.urlPrefix = urlPrefix;
  }

  public void addComponent(String area, VCMSComponentDescriptionXML component) {
    addComponent(area, component, false);
  }

  public void addComponent(String area, VCMSComponentDescriptionXML component,
      boolean mainComponent) {
    if (!components.containsKey(area)) {
      components.put(area, new ArrayList<>());
    }
    components.get(area).add(component);
    if (mainComponent) {
      mainComponentClassName = component.getImplementationClass();
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see de.vegetweb.vct.description.VCMSPageDescription#getComponents()
   */
  @Override
  public Map<String, List<VCMSComponentDescriptionXML>> getComponents() {
    return components;
  }

  public void setComponents(Map<String, List<VCMSComponentDescriptionXML>> components) {
    this.components = components;
  }

  /*
   * (non-Javadoc)
   *
   * @see de.vegetweb.vct.description.VCMSPageDescription#getLayout()
   */
  @Override
  public String getLayout() {
    return layout;
  }

  public void setLayout(String layout) {
    this.layout = layout;
  }

  /*
   * (non-Javadoc)
   *
   * @see de.vegetweb.vct.description.VCMSPageDescription#getMainComponentClassName ()
   */
  @Override
  public String getMainComponentClassName() {
    return mainComponentClassName;
  }

  public void setMainComponentClassName(String mainComponentClassName) {
    this.mainComponentClassName = mainComponentClassName;
  }

  @Override
  public boolean isAnonymousAllowed() {
    return anonymousAllowed;
  }

  public void setAnonymousAllowed(boolean anonymousAllowed) {
    this.anonymousAllowed = anonymousAllowed;
  }

  @Override
  public int hashCode() {
    return VCMSPageDescriptionXMLBeanUtil.doToHashCode(this);
  }

  @Override
  public boolean equals(Object obj) {
    return VCMSPageDescriptionXMLBeanUtil.doEquals(this, obj);
  }

  @Override
  public String toString() {
    return VCMSPageDescriptionXMLBeanUtil.doToString(this);
  }

}
