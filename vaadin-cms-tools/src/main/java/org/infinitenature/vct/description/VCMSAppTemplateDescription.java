package org.infinitenature.vct.description;

import java.util.List;
import java.util.Map;

public interface VCMSAppTemplateDescription {

  String getImplementationClass();

  Map<String, List<VCMSComponentDescriptionXML>> getStaticComponents();
}
