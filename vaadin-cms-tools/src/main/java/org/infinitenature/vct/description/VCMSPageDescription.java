package org.infinitenature.vct.description;

import java.util.List;
import java.util.Map;

public interface VCMSPageDescription {

  String getTitle();

  String getUrlPrefix();

  Map<String, List<VCMSComponentDescriptionXML>> getComponents();

  String getLayout();

  String getMainComponentClassName();

  boolean isAnonymousAllowed();
}
