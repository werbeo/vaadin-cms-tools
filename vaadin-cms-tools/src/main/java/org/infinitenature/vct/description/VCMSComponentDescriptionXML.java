package org.infinitenature.vct.description;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "componentDescription")
public class VCMSComponentDescriptionXML implements VCMSComponentDescription {
  @XmlAttribute(name = "implementationClass")
  private String implementationClass;
  @XmlElement(name = "parameter")
  private Map<String, String> parameter = new HashMap<>();

  public VCMSComponentDescriptionXML() {
    super();
  }

  public VCMSComponentDescriptionXML(String implementationClass) {
    this();
    this.implementationClass = implementationClass;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.vegetweb.vct.description.VCMSComponentDescription#getImplementationClass()
   */
  @Override
  public String getImplementationClass() {
    return implementationClass;
  }

  public void setImplementationClass(String implementationClass) {
    this.implementationClass = implementationClass;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.vegetweb.vct.description.VCMSComponentDescription#getParameter()
   */
  @Override
  public Map<String, String> getParameter() {
    return parameter;
  }

  public void setParameter(Map<String, String> parameter) {
    this.parameter = parameter;
  }

}
