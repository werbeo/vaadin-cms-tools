/**
 * This package contains the descriptions which need to be persisted in e.g. a database.
 * 
 * @author dve
 *
 */
package org.infinitenature.vct.description;
