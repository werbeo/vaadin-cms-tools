package org.infinitenature.vct.description;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "pageTemplateDescription")
public class VCMSAppTemplateDescriptionXML implements VCMSAppTemplateDescription {
  @XmlAttribute(name = "implementationClass")
  private String implementationClass;

  @XmlElement(name = "staticComponents")
  private Map<String, List<VCMSComponentDescriptionXML>> staticComponents = new HashMap<>();

  @Override
  public String getImplementationClass() {
    return implementationClass;
  }

  public void setImplementationClass(String implementationClass) {
    this.implementationClass = implementationClass;
  }

  @Override
  public Map<String, List<VCMSComponentDescriptionXML>> getStaticComponents() {
    return staticComponents;
  }

  public void setStaticComponents(Map<String, List<VCMSComponentDescriptionXML>> staticComponents) {
    this.staticComponents = staticComponents;
  }

  public void addStaticComponent(String area, VCMSComponentDescriptionXML staticComponent) {
    if (!staticComponents.containsKey(area)) {
      staticComponents.put(area, new ArrayList<>());
    }
    staticComponents.get(area).add(staticComponent);
  }


}
