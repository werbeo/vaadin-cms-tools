package org.infinitenature.vct.description;

import java.util.Map;

public interface VCMSComponentDescription {

  String getImplementationClass();

  Map<String, String> getParameter();

}
