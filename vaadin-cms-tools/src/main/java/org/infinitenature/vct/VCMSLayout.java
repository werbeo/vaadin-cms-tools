package org.infinitenature.vct;

public interface VCMSLayout extends VCMSComponent {
  public void addComponent(String area, VCMSComponent component, boolean expand);
}
