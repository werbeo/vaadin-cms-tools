package org.infinitenature.vct;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;
import org.apache.commons.lang3.Validate;
import com.vaadin.server.VaadinSession;

public class VCMSContext {
  public static final String SESSION_KEY = "VCMS_CONTEXT";
  private VCMSApp app;
  private ViewMode viewMode;
  private boolean forceViewMode = false;
  private Authentication authentication = Authentication.ANONYMOUS;

  protected Set<ContextChangeListener> contextChangeListeners =
      Collections.newSetFromMap(new WeakHashMap<ContextChangeListener, Boolean>());
  protected Set<ViewModeChangeListener> viewModeChangeListeners =
      Collections.newSetFromMap(new WeakHashMap<ViewModeChangeListener, Boolean>());
  protected Set<LoginChangeListener> loginChangeListeners =
      Collections.newSetFromMap(new WeakHashMap<LoginChangeListener, Boolean>());

  public static VCMSContext getCurrent() {
    return (VCMSContext) VaadinSession.getCurrent().getAttribute(SESSION_KEY);
  }

  public VCMSApp getApp() {
    return app;
  }

  public void setApp(VCMSApp app) {
    Validate.notNull(app, "The VCMSapp must not be null.");
    this.app = app;
  }

  public ViewMode getViewMode() {
    return viewMode;
  }

  public void setViewMode(ViewMode viewMode) {
    this.viewMode = viewMode;
  }

  public synchronized void notifyViewModeChanged() {
    viewModeChangeListeners.forEach(listener -> listener.viewModeChanged(getViewMode()));
  }
  public boolean isForceViewMode() {
    return forceViewMode;
  }

  public void setForceViewMode(boolean forceViewMode) {
    this.forceViewMode = forceViewMode;
  }

  /**
   * Registers a new {@link ContextChangeListener}.
   * <p>
   * Do <b>not</b> use anonymous classes, because the listeners are stored in a {@link WeakHashMap}.
   * Use lambdas or real classes.
   * 
   * @param listener
   */
  public void addContextChangedListener(ContextChangeListener listener) {
    contextChangeListeners.add(listener);
  }

  public boolean removeContextChangedListener(ContextChangeListener listener) {
    return contextChangeListeners.remove(listener);
  }

  /**
   * Registers a new {@link ViewModeChangeListener}.
   * <p>
   * Do <b>not</b> use anonymous classes, because the listeners are stored in a {@link WeakHashMap}.
   * Use lambdas or real classes.
   * 
   * @param listener
   */
  public void addViewModeChangedListener(ViewModeChangeListener listener) {
    viewModeChangeListeners.add(listener);
  }

  public boolean removeViewModeChangedListener(ViewModeChangeListener listener) {
    return viewModeChangeListeners.remove(listener);
  }

  /**
   * Registers a new {@link LoginChangeListener}.
   * <p>
   * Do <b>not</b> use anonymous classes, because the listeners are stored in a {@link WeakHashMap}.
   * Use lambdas or real classes.
   * 
   * @param listener
   */
  public void addLoginChangedListener(LoginChangeListener listener) {
    loginChangeListeners.add(listener);
  }

  public boolean removeLoginChangedListener(LoginChangeListener listener) {
    return loginChangeListeners.remove(listener);
  }

  public Authentication getAuthentication() {
    return authentication;
  }

  public void setAuthentication(Authentication authentication) {
    this.authentication = authentication;
  }
}
