package org.infinitenature.vct.theme;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;
import org.junit.Test;

public class WeakListenerTest {
  class ListenerImpl implements MyListener {
    @Override
    public void called() {
      System.out.println("ListenerImpl instance");
    }
  }

  @FunctionalInterface
  interface MyListener {
    public void called();
  }

  Set<MyListener> weakHashSet = Collections.newSetFromMap(new WeakHashMap<MyListener, Boolean>());

  @Test
  public void test() throws InterruptedException {
    weakHashSet.add(() -> System.out.println("Lambda expression"));
    weakHashSet.add(new MyListener() {
      @Override
      public void called() {
        System.out.println("Anonymous inner class");
      }
    });

    MyListener resizeListener = new ListenerImpl();
    weakHashSet.add(resizeListener);

    System.out.println("Before gc:");
    weakHashSet.forEach(listener -> listener.called());

    System.gc();
    Thread.sleep(1000);

    System.out.println("After gc:");
    weakHashSet.forEach(listener -> listener.called());
  }

}
