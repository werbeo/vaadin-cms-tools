package org.infinitenature.vct.theme;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.Properties;
import org.eclipse.jetty.server.Request;
import org.junit.Test;
import com.vaadin.server.DefaultDeploymentConfiguration;
import com.vaadin.server.ServiceException;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServletRequest;
import com.vaadin.server.VaadinServletService;

public class TestVaadinRequestUtil {

  @Test
  public void testGetHostname_Servlet() throws ServiceException {
    Request request = mock(Request.class);
    when(request.getServerName()).thenReturn("www.myhost.home");

    VaadinRequest vaadinRequest = new VaadinServletRequest(request, new VaadinServletService(null,
        new DefaultDeploymentConfiguration(getClass(), new Properties())));

    assertThat(VCMSUtil.getHostName(vaadinRequest), is(equalTo(("www.myhost.home"))));
  }
}
