package org.infinitenature.vct.description;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class TestVCMSPageDescriptionXML {
  private static final String EMPTY_AREA = "EMPTY_AREA";
  private static final String FILLED_AREA = "FILLED_AREA";
  private VCMSPageDescriptionXML pageDescritpion;

  @Before
  public void setUp() {
    pageDescritpion = new VCMSPageDescriptionXML();
    pageDescritpion.addComponent(FILLED_AREA, new VCMSComponentDescriptionXML(), true);
  }

  @Test
  public void testAddComponent_EmptyArea() {
    pageDescritpion.addComponent(EMPTY_AREA, new VCMSComponentDescriptionXML());

    assertTrue(pageDescritpion.getComponents().containsKey(EMPTY_AREA));
    assertThat(pageDescritpion.getComponents().get(EMPTY_AREA).size(), is(1));
  }

  @Test
  public void testAddComponent_FilledArea() {
    pageDescritpion.addComponent(FILLED_AREA, new VCMSComponentDescriptionXML());

    assertTrue(pageDescritpion.getComponents().containsKey(FILLED_AREA));
    assertThat(pageDescritpion.getComponents().get(FILLED_AREA).size(), is(2));
  }

  @Test
  public void testAddComponent_MainComponent() {
    final VCMSComponentDescriptionXML component = new VCMSComponentDescriptionXML("className");
    pageDescritpion.addComponent(FILLED_AREA, component, true);

    assertThat(pageDescritpion.getMainComponentClassName(), is("className"));
  }
}
