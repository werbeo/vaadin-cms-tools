package org.infinitenature.vct.vaadin.navigator;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.infinitenature.vct.Authentication;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.vct.VCMSLayout;
import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.vct.description.VCMSPageDescriptionXML;
import org.infinitenature.vct.error.NotLoggedInException;
import org.infinitenature.vct.repository.ComponentFactory;
import org.infinitenature.vct.repository.PageRepository;
import org.infinitenature.vct.theme.VCMSUtil;
import org.junit.Before;
import org.junit.Test;
import com.vaadin.server.ServiceException;
import com.vaadin.server.VaadinSession;

public class TestPageProvider {
  private PageRepository pageRepositoryMock;
  private PageProvider pageProviderUT;
  private ComponentFactory componentFactoryMock;
  private VCMSComponent componentMock;
  private VCMSLayout layoutMock;

  @Before
  public void setUp() throws ServiceException {
    VaadinSession mockSession = mock(VaadinSession.class);
    when(mockSession.getAttribute(VCMSContext.SESSION_KEY)).thenReturn(new VCMSContext());
    VaadinSession.setCurrent(mockSession);

    pageRepositoryMock = mock(PageRepository.class);
    VCMSPageDescriptionXML main = new VCMSPageDescriptionXML();
    main.setLayout("DEFAULT_LAYOUT");
    main.addComponent("MAIN", new VCMSComponentDescriptionXML("COMPONENT_CLASS"));
    when(pageRepositoryMock.getDescription(any(), eq("main"))).thenReturn(main);

    VCMSPageDescriptionXML restricted = new VCMSPageDescriptionXML();
    restricted.setLayout("DEFAULT_LAYOUT");
    restricted.setTitle("restricted");
    restricted.addComponent("MAIN", new VCMSComponentDescriptionXML("COMPONENT_CLASS"));
    restricted.setAnonymousAllowed(false);
    when(pageRepositoryMock.getDescription(any(), eq("restricted"))).thenReturn(restricted);

    when(pageRepositoryMock.getViewName(any(), eq("main"))).thenReturn("main");

    pageProviderUT = new PageProvider(pageRepositoryMock);
    componentFactoryMock = mock(ComponentFactory.class);

    componentMock = mock(VCMSComponent.class);
    layoutMock = mock(VCMSLayout.class);
    when(componentFactoryMock.get("COMPONENT_CLASS")).thenReturn(componentMock);
    when(componentFactoryMock.getLayout("DEFAULT_LAYOUT")).thenReturn(layoutMock);
    pageProviderUT.setComponentFactory(componentFactoryMock);

  }

  @Test
  public void testGetPage() {
    VCMSView mainView = pageProviderUT.getPage("main");
    verify(componentFactoryMock, times(1)).get("COMPONENT_CLASS");
    verify(componentFactoryMock, times(1)).getLayout("DEFAULT_LAYOUT");
  }

  @Test(expected = NotLoggedInException.class)
  public void testGetPage_NotLoggedIn() {
    pageProviderUT.getPage("restricted");
  }

  @Test()
  public void testGetPage_LoggedIn() {
    VCMSUtil.getCurrentContext().setAuthentication(new Authentication() {});
    VCMSView view = pageProviderUT.getPage("restricted");
    assertThat(view.getTitle(), is("restricted"));
  }

  @Test
  public void testForceView() {
    pageProviderUT.setForceViewName(Optional.of("tc"));
    assertThat(pageProviderUT.getViewName("main"), is("tc"));

    pageProviderUT.setForceViewName(Optional.empty());
    assertThat(pageProviderUT.getViewName("main"), is("main"));
  }
}
