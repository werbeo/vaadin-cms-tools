package org.infinitenature.vct;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class TestVCMSApp {


  @Test
  public void test_equals() {
    assertThat(new VCMSApp("App - A").equals(new VCMSApp("App - B")), is(false));
    assertThat(new VCMSApp("App - A").equals(new VCMSApp("App - A")), is(true));
  }

  @Test
  public void test_toString() {
    assertThat(new VCMSApp("App - A").toString(), containsString("App - A"));
  }

  @Test
  public void test_hashCode() {
    assertThat(new VCMSApp("App - A").hashCode(), is(not(new VCMSApp("App - B").hashCode())));
    assertThat(new VCMSApp("App - A").hashCode(), is(new VCMSApp("App - A").hashCode()));
  }
}
