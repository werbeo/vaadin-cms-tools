package org.infinitenature.vct.demo;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vct.LoginChangeListener;
import org.infinitenature.vct.VCMSContext;
import com.vaadin.server.VaadinSession;

public class DemoContext extends VCMSContext {

  public static DemoContext getCurrent() {
    return (DemoContext) VaadinSession.getCurrent().getAttribute(SESSION_KEY);
  }

  private String userName = null;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    if (!StringUtils.equals(this.userName, userName)) {
      this.userName = userName;
      loginChangeListeners.forEach(LoginChangeListener::loginChanged);
    }
  }
}
