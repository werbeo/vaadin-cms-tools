package org.infinitenature.vct.demo.repository;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.ViewMode;
import org.infinitenature.vct.demo.component.IPAppTemplateImpl;
import org.infinitenature.vct.demo.component.LocalhostAppTemplateImpl;
import org.infinitenature.vct.demo.component.VCMSNavigationComponent;
import org.infinitenature.vct.description.VCMSAppTemplateDescription;
import org.infinitenature.vct.description.VCMSAppTemplateDescriptionXML;
import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.vct.repository.AppTemplateRepositoryQueries;

public class MockAppTemplateRepo implements AppTemplateRepositoryQueries {

  private static final String NAVIGATION_COMPONENT_CLASS = VCMSNavigationComponent.class.getName();

  @Override
  public VCMSAppTemplateDescription getAppTemplate(VCMSApp app, ViewMode viewMode) {

    VCMSAppTemplateDescriptionXML template = new VCMSAppTemplateDescriptionXML();
    if(MockAppRepo.IP.equals(app.getName())) {
      template.setImplementationClass(IPAppTemplateImpl.class.getName());
    } else if (MockAppRepo.LOCALHOST.equals(app.getName())) {
      template.setImplementationClass(LocalhostAppTemplateImpl.class.getName());
    } else {
      throw new IllegalStateException("No app template defined for " +app.getName());
    }
    template.addStaticComponent("LEFT", navigationComponent());
    template.addStaticComponent("RIGHT", MockRepository.htmlComponent("<h2>RIGHT</h2>"));
    return template;
  }

  private VCMSComponentDescriptionXML navigationComponent() {
    return new VCMSComponentDescriptionXML(NAVIGATION_COMPONENT_CLASS);
  }
}
