package org.infinitenature.vct.demo.component;

import java.util.HashMap;
import java.util.Map;
import org.infinitenature.vct.TitleUpdateListener;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;

public class ProfileComponent extends VCMSHtmlComponent {

  private TitleUpdateListener titleUpdateListener;
  private Map<String, String> titleParamters = new HashMap<>();

  @Override
  public void init(Map<String, String> parameter) {
    parameter.put(VCMSHtmlComponent.PARAMETER_CONTENT, "Profile");
    super.init(parameter);
  }

  @Override
  public void enter(ViewChangeEvent event) {
    System.out.println("Loading profile for " + event.getParameters());
    titleParamters.put("PROFILE", event.getParameters());
    titleUpdateListener.onTitleParameterUpdate();
  }

  @Override
  public void setInitialParameters(String parameters) {
    titleParamters.put("PROFILE", parameters);
  }

  @Override
  public Map<String, String> getTitleParameters() {
    return titleParamters;
  }

  @Override
  public void setTitleUpdateListener(TitleUpdateListener titleUpdateListener) {
    this.titleUpdateListener = titleUpdateListener;
  }
}
