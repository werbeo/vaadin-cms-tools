package org.infinitenature.vct.demo;

import org.jsoup.nodes.Element;
import com.vaadin.server.BootstrapFragmentResponse;
import com.vaadin.server.BootstrapListener;
import com.vaadin.server.BootstrapPageResponse;

public class ViewportBootstrapListener implements BootstrapListener {
  @Override
  public void modifyBootstrapFragment(BootstrapFragmentResponse response) {
    // NOOP
  }

  @Override
  public void modifyBootstrapPage(BootstrapPageResponse response) {
    Element head = response.getDocument().getElementsByTag("head").get(0);
    Element meta = head.appendElement("meta");
    meta.attr("name", "viewport");
    meta.attr("content", "width=device-width, initial-scale=1");
  }
}