package org.infinitenature.vct.demo;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.demo.repository.MockAppTemplateRepo;
import org.infinitenature.vct.demo.repository.MockComponentFactory;
import org.infinitenature.vct.demo.repository.MockRepository;
import org.infinitenature.vct.repository.AppTemplateFactory;
import org.infinitenature.vct.repository.AppTemplateRepositoryQueries;
import org.infinitenature.vct.vaadin.AbstractViewModeDetector;
import org.infinitenature.vct.vaadin.navigator.PageProvider;
import com.vaadin.server.Page.BrowserWindowResizeEvent;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import de.akquinet.engineering.vaadin.HistoryApiNavigatorFactory;

/**
 * This UI is the application entry point. A UI may either represent a browser window (or tab) or
 * some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
public class MyUI extends UI {

  @Override
  protected void init(VaadinRequest vaadinRequest) {

    MockComponentFactory componentFactory = new MockComponentFactory();
    AppTemplateRepositoryQueries appTemplateRepositoryQueries = new MockAppTemplateRepo();
    AppTemplateFactory appTemplateFactory =
        new AppTemplateFactory(appTemplateRepositoryQueries, componentFactory);


    VCMSApp app = DemoContext.getCurrent().getApp();
    org.infinitenature.vct.ViewMode viewMode = DemoContext.getCurrent().getViewMode();

    PageProvider viewProvider = new PageProvider(new MockRepository());
    viewProvider.setComponentFactory(componentFactory);
    VCMSAppTemplate appTemplate = appTemplateFactory.getVCMSAppTemplate(app, viewMode);
    setNavigator(HistoryApiNavigatorFactory.createHistoryApiNavigator(this,
        appTemplate));

    getNavigator().addProvider(viewProvider);
    getNavigator().setErrorProvider(viewProvider.getErrorViewProvider());



    getPage().addBrowserWindowResizeListener(new AbstractViewModeDetector() {

      @Override
      public ViewMode getNewViewMode(BrowserWindowResizeEvent event) {
        if (event.getWidth() < 768) {
          return ViewMode.MOBILE;
        } else {
          return ViewMode.DESKTOP;
        }
      }
    });
    setContent(appTemplate.getVaadinComponent());
  }


}
