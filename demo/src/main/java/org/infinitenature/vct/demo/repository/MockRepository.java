package org.infinitenature.vct.demo.repository;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.demo.component.ProfileComponent;
import org.infinitenature.vct.demo.component.VCMSHtmlComponent;
import org.infinitenature.vct.demo.component.VCMSLoginComponent;
import org.infinitenature.vct.demo.layout.ThreeColumnLayout;
import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.vct.description.VCMSPageDescription;
import org.infinitenature.vct.description.VCMSPageDescriptionXML;
import org.infinitenature.vct.repository.PageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockRepository implements PageRepository {
  private static final Logger LOGGER = LoggerFactory.getLogger(MockRepository.class);

  private static final String HTML_COMPONENT_CLASS = VCMSHtmlComponent.class.getName();
  private static final String THREE_COLUM_LAYOUT_CLASS = ThreeColumnLayout.class.getName();
  private static final String URL_PROFILE = "profile";
  private static final String URL_HELLO = "hello";
  private static final String URL_MAIN = "main";
  private static final String URL_NON_ANON = "needsLogin";

  @Override
  public VCMSPageDescription getDescription(VCMSApp app, String viewName) {
    LOGGER.info("Reququesting page description for app: {}, and view name: {}", app, viewName);
    switch (viewName) {
      case URL_MAIN:
        return mainPageDescription(app);
      case URL_HELLO:
        return helloPageDescription();
      case URL_PROFILE:
        return profilePageDescription();
      case URL_NON_ANON:
        return needsLoginPageDescription();
      default:
        throw new IllegalArgumentException("No PageDescription for " + viewName);
    }
  }

  @Override
  public String getFragment(VCMSApp app, Class<? extends VCMSComponent> mainComponentClass) {
    LOGGER.info(mainComponentClass.getName());
    LOGGER.info(profilePageDescription().getMainComponentClassName());
    if (profilePageDescription().getMainComponentClassName().equals(mainComponentClass.getName())) {
      LOGGER.info(profilePageDescription().getUrlPrefix());
      return profilePageDescription().getUrlPrefix();
    } else {
      return URL_MAIN;
    }
  }



  private VCMSPageDescription mainPageDescription(VCMSApp app) {
    VCMSPageDescriptionXML mainPageDescription = new VCMSPageDescriptionXML();
    mainPageDescription.setUrlPrefix(URL_MAIN);
    mainPageDescription.setTitle("This is the home page for the app: " + app.getName());
    mainPageDescription.setLayout(THREE_COLUM_LAYOUT_CLASS);
    mainPageDescription.addComponent("CENTER",
        htmlComponent("<h1>Welcome</h1>\nThis works!\n<a href=\"hello\">LINK</a>"), true);

    mainPageDescription.addComponent("RIGHT",
        new VCMSComponentDescriptionXML(VCMSLoginComponent.class.getName()));

    return mainPageDescription;
  }

  private VCMSPageDescription profilePageDescription() {
    VCMSPageDescriptionXML profilePageDescription = new VCMSPageDescriptionXML();
    profilePageDescription.setUrlPrefix(URL_PROFILE);
    profilePageDescription.setTitle("This is the profile page for ${PROFILE}");
    profilePageDescription.setLayout(THREE_COLUM_LAYOUT_CLASS);
    profilePageDescription.addComponent("CENTER",
        new VCMSComponentDescriptionXML(ProfileComponent.class.getName()), true);

    return profilePageDescription;
  }

  public static VCMSComponentDescriptionXML htmlComponent(String content) {
    VCMSComponentDescriptionXML vcmsComponentDescription =
        new VCMSComponentDescriptionXML(HTML_COMPONENT_CLASS);

    vcmsComponentDescription.getParameter().put(VCMSHtmlComponent.PARAMETER_CONTENT, content);
    return vcmsComponentDescription;
  }



  private VCMSPageDescription helloPageDescription() {
    VCMSPageDescriptionXML helloPageDescription = new VCMSPageDescriptionXML();
    helloPageDescription.setUrlPrefix(URL_HELLO);
    helloPageDescription.setTitle("This is the hello page");
    helloPageDescription.setLayout(THREE_COLUM_LAYOUT_CLASS);
    VCMSComponentDescriptionXML vcmsComponentDescription =
        new VCMSComponentDescriptionXML(HTML_COMPONENT_CLASS);
    vcmsComponentDescription.getParameter().put(VCMSHtmlComponent.PARAMETER_CONTENT,
        "<h1>This is the hello Page</h1>\nIt still works!");
    helloPageDescription.addComponent("LEFT", vcmsComponentDescription, true);
    return helloPageDescription;
  }

  private VCMSPageDescription needsLoginPageDescription() {
    VCMSPageDescriptionXML helloPageDescription = new VCMSPageDescriptionXML();
    helloPageDescription.setUrlPrefix(URL_NON_ANON);
    helloPageDescription.setTitle("TProtected page page");
    helloPageDescription.setLayout(THREE_COLUM_LAYOUT_CLASS);
    VCMSComponentDescriptionXML vcmsComponentDescription =
        new VCMSComponentDescriptionXML(HTML_COMPONENT_CLASS);
    vcmsComponentDescription.getParameter().put(VCMSHtmlComponent.PARAMETER_CONTENT,
        "<h1>This is a page wich needs login</h1>\nIt still works!");
    helloPageDescription.addComponent("LEFT", vcmsComponentDescription);
    helloPageDescription.setAnonymousAllowed(false);
    return helloPageDescription;
  }

  @Override
  public String getViewName(VCMSApp app, String viewAndParameters) {
    if (viewAndParameters.startsWith(URL_MAIN)) {
      return URL_MAIN;
    } else if (viewAndParameters.startsWith(URL_HELLO)) {
      return URL_HELLO;
    } else if (viewAndParameters.startsWith(URL_PROFILE)) {
      return URL_PROFILE;
    } else if (viewAndParameters.startsWith(URL_NON_ANON)) {
      return URL_NON_ANON;
    } else if (viewAndParameters.isEmpty()) {
      return URL_MAIN;
    } else {
      return null;
    }
  }
}
