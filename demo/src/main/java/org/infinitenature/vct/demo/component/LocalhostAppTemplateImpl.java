package org.infinitenature.vct.demo.component;

import java.util.Map;
import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.VCMSComponent;
import com.vaadin.navigator.View;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class LocalhostAppTemplateImpl implements VCMSAppTemplate {

  private VerticalLayout left = new VerticalLayout();
  private VerticalLayout right = new VerticalLayout();

  private Component dynamicContentComponent = new Panel();

  private Layout layout = new VerticalLayout(new Label("LOCALHOST - The app"),
      new HorizontalLayout(left, dynamicContentComponent, right));

  @Override
  public Component getVaadinComponent() {
    return layout;
  }

  @Override
  public void init(Map<String, String> parameter) {
    // NOOP

  }

  @Override
  public void addTemplateComponent(String area, VCMSComponent component) {
    if ("LEFT".equals(area)) {
      left.addComponent(component.getVaadinComponent());
    } else if ("RIGHT".equals(area)) {
      right.addComponent(component.getVaadinComponent());
    }

  }

  @Override
  public void showView(View view) {
    layout.replaceComponent(dynamicContentComponent, view.getViewComponent());
    dynamicContentComponent = view.getViewComponent();
  }
}
