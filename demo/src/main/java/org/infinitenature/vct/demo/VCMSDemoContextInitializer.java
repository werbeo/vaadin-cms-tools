package org.infinitenature.vct.demo;

import org.infinitenature.vct.VCMSContext;
import org.infinitenature.vct.VCMSContextInitializer;
import com.vaadin.server.SessionInitEvent;

public class VCMSDemoContextInitializer extends VCMSContextInitializer {

  @Override
  protected VCMSContext createContextInstance(SessionInitEvent event) {
    return new DemoContext();
  }
}
