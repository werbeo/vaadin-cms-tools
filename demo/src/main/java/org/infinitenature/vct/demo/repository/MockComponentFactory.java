package org.infinitenature.vct.demo.repository;

import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSLayout;
import org.infinitenature.vct.repository.ComponentFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockComponentFactory implements ComponentFactory {
  private static final Logger LOGGER = LoggerFactory.getLogger(MockComponentFactory.class);

  @Override
  public VCMSComponent get(String implementationClass) {
    LOGGER.info("Requesting component of type: {}", implementationClass);
    try {
      Class<?> c = Class.forName(implementationClass);
      return (VCMSComponent) c.newInstance();
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
      LOGGER.error("Failure getting component for class {}", implementationClass, e);
      return null;
    }
  }

  @Override
  public VCMSLayout getLayout(String layout) {
    LOGGER.info("Requesting layout of type: {}", layout);
    try {
      Class<?> c = Class.forName(layout);
      return (VCMSLayout) c.newInstance();
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
      LOGGER.error("Failure getting layout {}", layout, e);
      return null;
    }
  }

  @Override
  public VCMSAppTemplate getAppTemplate(String appTemplateImplementationClass) {
    LOGGER.info("Requesting app template of type: {}", appTemplateImplementationClass);
    try {
      Class<?> c = Class.forName(appTemplateImplementationClass);
      return (VCMSAppTemplate) c.newInstance();
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
      LOGGER.error("Failure getting app template for class {}", appTemplateImplementationClass, e);
      return null;
    }
  }

  @Override
  public <T extends VCMSComponent> T getComponent(Class<T> clazz) {
    LOGGER.info("Requesting component of type: {}", clazz);
    try {
      return clazz.newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      LOGGER.error("Failure getting component for class {}", clazz, e);
      return null;
    }
  }
}
