package org.infinitenature.vct.demo.component;

import java.util.Map;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.vct.ViewMode;
import org.infinitenature.vct.ViewModeChangeListener;
import org.infinitenature.vct.demo.repository.MockRepository;
import org.infinitenature.vct.repository.URLRepository;
import org.infinitenature.vct.theme.VCMSUtil;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class VCMSHtmlComponent implements VCMSComponent, ViewModeChangeListener {
  public static final String PARAMETER_CONTENT = "content";
  private Map<String, String> parameter;
  private Label label = new Label("", ContentMode.HTML);
  private Label viewModeLabel = new Label();
  private URLRepository urlRepo = new MockRepository();

  @Override
  public Component getVaadinComponent() {
    Button button = new Button("Profile", event -> {
      UI.getCurrent().getNavigator().navigateTo(
          urlRepo.getFragment(VCMSUtil.getCurrentContext().getApp(), ProfileComponent.class)
              + "/2");
    });
    button.setSizeFull();
    VerticalLayout verticalLayout = new VerticalLayout(label, button, viewModeLabel);
    verticalLayout.setSizeFull();
    return verticalLayout;
  }

  @Override
  public void init(Map<String, String> parameter) {
    this.parameter = parameter;
    label.setValue(parameter.get(PARAMETER_CONTENT));
    label.setSizeFull();
    VCMSContext.getCurrent().addViewModeChangedListener(this);
    viewModeChanged(VCMSContext.getCurrent().getViewMode());

  }

  @Override
  public void viewModeChanged(ViewMode newViewMode) {
    if (org.infinitenature.vct.demo.ViewMode.DESKTOP == newViewMode) {
      viewModeLabel.setValue("DESKTOP");
    } else {
      viewModeLabel.setValue("MOBILE");
    }
  }

}
