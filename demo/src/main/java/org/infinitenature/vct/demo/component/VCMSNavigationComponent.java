package org.infinitenature.vct.demo.component;

import java.util.Map;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.demo.repository.MockRepository;
import org.infinitenature.vct.repository.URLRepository;
import org.infinitenature.vct.theme.VCMSUtil;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class VCMSNavigationComponent implements VCMSComponent {

  private VerticalLayout layout = new VerticalLayout();
  private URLRepository urlRepo = new MockRepository();

  public VCMSNavigationComponent() {
    Button profileButton = new Button("Profile", event -> {
      UI.getCurrent().getNavigator().navigateTo(
          urlRepo.getFragment(VCMSUtil.getCurrentContext().getApp(), ProfileComponent.class)
              + "/2");
    });

    Button homeButton = new Button("Home", event -> {
      UI.getCurrent().getNavigator().navigateTo("");
    });

    Button helloButton = new Button("Hello", event -> {
      UI.getCurrent().getNavigator().navigateTo("hello");
    });

    layout.addComponents(homeButton, helloButton, profileButton);
  }

  @Override
  public Component getVaadinComponent() {
    return layout;
  }

  @Override
  public void init(Map<String, String> parameter) {
    // NOOP
  }

}
