package org.infinitenature.vct.demo.component;

import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vct.LoginChangeListener;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.demo.DemoContext;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class VCMSLoginComponent implements VCMSComponent, LoginChangeListener {

  private Button admin = new Button("Login as Admin");
  private Button user = new Button("Login as User");
  private Button logout = new Button("Logout");
  private VerticalLayout layout = new VerticalLayout();

  public VCMSLoginComponent() {
    admin.addClickListener(event -> DemoContext.getCurrent().setUserName("ADMIN"));
    user.addClickListener(event -> DemoContext.getCurrent().setUserName("USER"));
    logout.addClickListener(event -> DemoContext.getCurrent().setUserName(null));
  }

  @Override
  public Component getVaadinComponent() {

    return layout;
  }

  @Override
  public void init(Map<String, String> parameter) {
    DemoContext context = DemoContext.getCurrent();
    context.addLoginChangedListener(this);
    loginChanged();

  }

  @Override
  public void loginChanged() {
    DemoContext context = DemoContext.getCurrent();
    layout.removeAllComponents();
    if (StringUtils.isBlank(context.getUserName())) {
      layout.addComponent(user);
      layout.addComponent(admin);
    } else if (StringUtils.equals("ADMIN", context.getUserName())) {
      layout.addComponent(new Label("Logged in as admin"));
      layout.addComponent(logout);
    } else if (StringUtils.equals("USER", context.getUserName())) {
      layout.addComponent(new Label("Logged in as user"));
      layout.addComponent(logout);
    }
  }

}
