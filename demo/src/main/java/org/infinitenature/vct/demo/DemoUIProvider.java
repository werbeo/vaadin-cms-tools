package org.infinitenature.vct.demo;

import org.infinitenature.vct.theme.ThemeByHostnameUIProvider;

public class DemoUIProvider extends ThemeByHostnameUIProvider {
  @Override
  public String getThemeName(String hostName) {
    switch (hostName) {
      case "localhost":
        return "mytheme";
      default:
        return "valo";
    }
  }
}
