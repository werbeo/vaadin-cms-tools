package org.infinitenature.vct.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import org.infinitenature.vct.VCMSContextInitializer;
import org.infinitenature.vct.demo.repository.MockAppRepo;
import org.infinitenature.vct.demo.repository.MockComponentFactory;
import org.infinitenature.vct.demo.repository.MockRepository;
import org.infinitenature.vct.vaadin.SeoBootstrapListener;
import org.infinitenature.vct.vaadin.navigator.PageProvider;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;

@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
@WebServlet(urlPatterns = "/*", name = "DemoServlet", asyncSupported = true)
public class DemoServlet extends VaadinServlet {
  private final DemoUIProvider uiProvider = new DemoUIProvider();
  private final VCMSContextInitializer contextInitializer = new VCMSDemoContextInitializer();

  public DemoServlet() {
    contextInitializer.setAppRepository(new MockAppRepo());
  }

  @Override
  protected void servletInitialized() throws ServletException {
    super.servletInitialized();

    getService().addSessionInitListener(event -> {
      event.getSession().addUIProvider(uiProvider);
    });

    PageProvider pageProvider = new PageProvider(new MockRepository());
    MockComponentFactory componentFactory = new MockComponentFactory();
    pageProvider.setComponentFactory(componentFactory);

    getService().addSessionInitListener(contextInitializer);
    VaadinService.getCurrent().addSessionInitListener((com.vaadin.server.SessionInitEvent e) -> e
        .getSession().addBootstrapListener(new ViewportBootstrapListener()));
    VaadinService.getCurrent().addSessionInitListener(
        (com.vaadin.server.SessionInitEvent e) -> e.getSession().addBootstrapListener(
            new SeoBootstrapListener(pageProvider)));
  }
}
