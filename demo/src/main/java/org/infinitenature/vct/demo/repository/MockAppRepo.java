package org.infinitenature.vct.demo.repository;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.repository.AppRepositoryQueries;

public class MockAppRepo implements AppRepositoryQueries {

  public static final String IP = "ip";
  public static final String LOCALHOST = "localhost";

  @Override
  public VCMSApp getApp(String hostName) {
    VCMSApp app = new VCMSApp();
    if (LOCALHOST.equalsIgnoreCase(hostName)) {
      app.setName(LOCALHOST);
      app.setDescription("This is the demo app with localhost as hostname");
    } else if ("127.0.0.1".equalsIgnoreCase(hostName)) {
      app.setDescription("This is the demo app with 127.0.0.1 as hostname");
      app.setName(IP);
    } else {
      throw new IllegalArgumentException("No app for host " + hostName + " defined.");
    }
    return app;
  }

}
