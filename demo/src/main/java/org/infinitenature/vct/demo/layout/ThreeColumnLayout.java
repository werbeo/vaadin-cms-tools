package org.infinitenature.vct.demo.layout;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSLayout;
import com.jarektoro.responsivelayout.ResponsiveColumn;
import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public class ThreeColumnLayout implements VCMSLayout {
  public final static String AREA_LEFT = "LEFT";
  public final static String AREA_CENTER = "CENTER";
  public final static String AREA_RIGHT = "RIGHT";
  private ResponsiveLayout layout = new ResponsiveLayout();
  private ResponsiveColumn right;
  private ResponsiveColumn center;
  private ResponsiveColumn left;

  private VerticalLayout leftLayout = new VerticalLayout();
  private VerticalLayout centerLayout = new VerticalLayout();
  private VerticalLayout rightLayout = new VerticalLayout();

  private Set<VCMSComponent> components = new HashSet<>();

  public ThreeColumnLayout() {
    ResponsiveRow row = layout.addRow();

    left = row.addColumn().withDisplayRules(12, 6, 3, 4);
    left.setComponent(leftLayout);
    center = row.addColumn().withDisplayRules(12, 6, 6, 4);
    center.setComponent(centerLayout);
    right = row.addColumn().withDisplayRules(12, 6, 3, 4);
    right.setComponent(rightLayout);
  }

  @Override
  public Component getVaadinComponent() {
    return layout;
  }

  @Override
  public void init(Map<String, String> parameter) {
    // TODO Auto-generated method stub

  }

  @Override
  public void enter(ViewChangeEvent event) {
    components.forEach(component -> component.enter(event));

  }

  @Override
  public void addComponent(String area, VCMSComponent component, boolean expand) {
    Component vaadinComponent = component.getVaadinComponent();
    components.add(component);
    vaadinComponent.setSizeFull();
    switch (area) {
      case AREA_LEFT:
        leftLayout.addComponent(vaadinComponent);
        break;
      case AREA_CENTER:
        centerLayout.addComponent(vaadinComponent);
        break;
      case AREA_RIGHT:
        rightLayout.addComponent(vaadinComponent);
        break;
      default:
        break;
    }

  }

}
